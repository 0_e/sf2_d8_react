import React from 'react'
import { Route, IndexRoute, Redirect } from 'react-router'

import CoreLayout from 'layouts/CoreLayout/CoreLayout'
import TodoView from 'views/TodoView/TodoView'
import NotFoundView from 'views/NotFoundView/NotFoundView'
import ArticleView from 'views/ArticleView/ArticleView'

export default (store) => (
  <Route path='/' component={CoreLayout}>
    <IndexRoute component={TodoView} />
    <Route path='/articles' component={ArticleView} />
    <Route path='/404' component={NotFoundView} />
    <Redirect from='*' to='/404' />
  </Route>
)
