import fetch from 'isomorphic-fetch'

// ------------------------------------
// Constants
// ------------------------------------
export const FETCH_ARTICLES = 'FETCH_ARTICLES'
export const RECEIVE_ARTICLES = 'RECEIVE_ARTICLES'

// ------------------------------------
// Actions
// ------------------------------------
function receiveArticles (articles) {
  return {
    type: 'RECEIVE_ARTICLES',
    articles
  }
}

export const fetchArticles = () => {
  return (dispatch) => {
    dispatch({
      type: 'FETCH_ARTICLES'
    })

    fetch('http://techdemocz92zzvr7p.devcloud.acquia-sites.com/rest/view/articles')
      .then(req => req.json())
      .then(json => dispatch(receiveArticles(json)))
  }
}

const articles = (state = [], action) => {
  switch (action.type) {
    case 'RECEIVE_ARTICLES':
      return action.articles

    default:
      return state
  }
}

export default articles
