import fetch from 'isomorphic-fetch'

// ------------------------------------
// Constants
// ------------------------------------
export const FETCH_TODOS = 'FETCH_TODOS'
export const RECEIVE_TODOS = 'RECEIVE_TODOS'
export const RECEIVE_TODO = 'RECEIVE_TODOS'
export const ADD_TODO = 'ADD_TODO'
export const REMOVE_TODO = 'REMOVE_TODO'

// ------------------------------------
// Actions
// ------------------------------------
function receiveTodos (todos) {
  return {
    type: 'RECEIVE_TODOS',
    todos
  }
}

export const fetchTodos = () => {
  return (dispatch) => {
    dispatch({
      type: 'FETCH_TODOS'
    })

    fetch('http://localhost:8000/api/todos')
      .then(req => req.json())
      .then(json => dispatch(receiveTodos(json)))
  }
}

function receiveTodo (todo, remove = false, removeId) {
  console.log('here', remove)
  return {
    type: 'RECEIVE_TODO',
    todo,
    removeId
  }
}

export const addTodo = (title, description) => {
  return (dispatch) => {
    dispatch({
      type: 'ADD_TODO'
    })

    fetch('http://localhost:8000/api/todos', {
      method: 'post',
      mode: 'cors',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        'todo': {
          title,
          description
        }
      })
    })
    .then(req => req.json())
    .then(json => dispatch(receiveTodo(json)))
  }
}

export const removeTodo = (id) => {
  return (dispatch) => {
    dispatch({
      type: 'REMOVE_TODO'
    })

    fetch(`http://localhost:8000/api/todos/${id}`, {
      method: 'delete',
      mode: 'cors'
    })
    .then(req => req.json())
    .then(json => dispatch(receiveTodo(json, true, id)))
  }
}

const todos = (state = [], action) => {
  switch (action.type) {
    case 'RECEIVE_TODOS':
      return action.todos

    case 'RECEIVE_TODO':
      if (typeof action.removeId !== 'undefined') {
        return state.filter((item) => {
          return item.id !== action.removeId
        })
      }

      return [
        ...state,
        action.todo
      ]

    default:
      return state
  }
}

export default todos
