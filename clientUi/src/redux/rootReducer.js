import { combineReducers } from 'redux'
import { routeReducer as router } from 'react-router-redux'
import todo from './modules/todo'
import article from './modules/article'

export default combineReducers({
  todo,
  router,
  article
})
