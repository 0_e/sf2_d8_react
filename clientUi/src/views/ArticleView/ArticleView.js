import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { fetchArticles } from '../../redux/modules/article'
import Articles from '../../components/Articles/Articles'

export class ArticleView extends React.Component {
  static propTypes = {
    article: PropTypes.array.isRequired,
    fetchArticlesAction: PropTypes.func.isRequired
  };

  componentDidMount () {
    this.props.fetchArticlesAction()
  }

  render () {
    return (
      <Articles articles={this.props.article} />
    )
  }
}

const mapStateToProps = (state) => ({
  article: state.article
})

const mapDispatchToProps = (dispatch) => {
  return {
    fetchArticlesAction: () => {
      dispatch(fetchArticles())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticleView)
