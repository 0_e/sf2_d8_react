import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { addTodo, removeTodo, fetchTodos } from '../../redux/modules/todo'
import Todos from '../../components/Todos/Todos'
import AddTodo from '../../components/AddTodo/AddTodo'

export class TodoView extends React.Component {
  static propTypes = {
    todo: PropTypes.array.isRequired,
    removeTodoAction: PropTypes.func.isRequired,
    addTodoAction: PropTypes.func.isRequired,
    fetchTodosAction: PropTypes.func.isRequired
  };

  componentDidMount () {
    this.props.fetchTodosAction()
  }

  render () {
    return (
      <div>
        <Todos todos={this.props.todo} removeTodo={this.props.removeTodoAction} />
        <AddTodo addTodo={this.props.addTodoAction} />
      </div>
    )
  }
}

const mapStateToProps = (state) => ({
  todo: state.todo
})

const mapDispatchToProps = (dispatch) => {
  return {
    removeTodoAction: (id) => {
      dispatch(removeTodo(id))
    },
    addTodoAction: (title, description) => {
      dispatch(addTodo(title, description))
    },
    fetchTodosAction: () => {
      dispatch(fetchTodos())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoView)
