import React, { PropTypes, Component } from 'react'
import { Table } from 'react-bootstrap'
import Todo from './Todo/Todo'

export default class Todos extends Component {
  render () {
    if (!this.props.todos) {
      return (<div>No Todos</div>)
    }

    return (
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Title</th>
            <th>Description</th>
            <th>&nbsp;</th>
          </tr>
        </thead>
        <tbody>
          {this.props.todos.map((todo, i) =>
            <Todo key={i} removeTodo={this.props.removeTodo} todo={todo} />
          )}
        </tbody>
      </Table>
    )
  }
}

Todos.propTypes = {
  todos: PropTypes.array.isRequired,
  removeTodo: PropTypes.func.isRequired
}
