import React, { PropTypes, Component } from 'react'
import { Button } from 'react-bootstrap'

export default class Todo extends Component {
  constructor () {
    super()
    this.handleRemoveItem.bind(this)
  }

  handleRemoveItem () {
    this.props.removeTodo(this.props.todo.id)
  }

  render () {
    let todo = this.props.todo

    return (
      <tr>
        <td>{todo.id}</td>
        <td>{todo.title}</td>
        <td>{todo.description}</td>
        <td><Button bsStyle='danger' onClick={this.handleRemoveItem.bind(this, todo.id)}>Remove</Button></td>
      </tr>
    )
  }
}

Todo.propTypes = {
  todo: PropTypes.object.isRequired,
  removeTodo: PropTypes.func.isRequired
}
