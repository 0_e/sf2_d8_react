import React, { PropTypes, Component } from 'react'
import { Button, Input } from 'react-bootstrap'

export default class AddTodo extends Component {
  constructor (props) {
    super(props)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
    this.handleTitleChange = this.handleTitleChange.bind(this)
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this)
    this.state = {
      'title': '',
      'description': ''
    }
  }

  handleFormSubmit () {
    this.props.addTodo(this.state.title, this.state.description)
  }

  handleTitleChange () {
    this.setState({
      title: this.refs.title.getValue()
    })
  }

  handleDescriptionChange () {
    this.setState({
      description: this.refs.description.getValue()
    })
  }

  render () {
    return (
      <form>
        <Input
          ref='title'
          type='text'
          label='Title'
          value={this.state.title}
          onChange={this.handleTitleChange}
        />
        <Input
          ref='description'
          type='text'
          label='Description'
          value={this.state.description}
          onChange={this.handleDescriptionChange}
        />
        <Button bsStyle='primary' onClick={this.handleFormSubmit}>Add</Button>
      </form>
    )
  }
}

AddTodo.propTypes = {
  addTodo: PropTypes.func.isRequired
}
