import React, { PropTypes, Component } from 'react'
import { Table } from 'react-bootstrap'
import Article from './Article/Article'

export default class Articles extends Component {
  render () {
    if (!this.props.articles) {
      return (<div>No Articles</div>)
    }

    return (
      <Table striped bordered condensed hover>
        <thead>
          <tr>
            <th>Title</th>
            <th>Content</th>
          </tr>
        </thead>
        <tbody>
          {this.props.articles.map((article, i) =>
            <Article key={i} article={article} />
          )}
        </tbody>
      </Table>
    )
  }
}

Articles.propTypes = {
  articles: PropTypes.array.isRequired
}
