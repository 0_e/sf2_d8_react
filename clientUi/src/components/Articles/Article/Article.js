import React, { PropTypes, Component } from 'react'

export default class Article extends Component {
  render () {
    let article = this.props.article
    let createMarkup = () => {
      return { __html: article.body[0].value }
    }

    return (
      <tr>
        <td>{article.title[0].value}</td>
        <td dangerouslySetInnerHTML={createMarkup()} />
      </tr>
    )
  }
}

Article.propTypes = {
  article: PropTypes.object.isRequired
}
