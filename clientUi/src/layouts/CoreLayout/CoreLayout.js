import React, { PropTypes } from 'react'
import { Navbar, Nav, NavItem, Row, Col } from 'react-bootstrap'
import '../../styles/core.scss'
import { LinkContainer } from 'react-router-bootstrap'

// Note: Stateless/function components *will not* hot reload!
// react-transform *only* works on component classes.
//
// Since layouts rarely change, they are a good place to
// leverage React's new Stateless Functions:
// https://facebook.github.io/react/docs/reusable-components.html#stateless-functions
//
// CoreLayout is a pure function of its props, so we can
// define it with a plain javascript function...
function CoreLayout ({ children }) {
  return (
    <div>
      <Navbar inverse fixedTop>
        <Navbar.Header>
          <Navbar.Brand>
            <a href='#'>Amido Brownbag</a>
          </Navbar.Brand>
          <Navbar.Toggle />
          <Nav>
            <LinkContainer to={{ pathname: '/' }}>
              <NavItem>Todos</NavItem>
            </LinkContainer>
            <LinkContainer to={{ pathname: '/articles' }}>
              <NavItem>Articles</NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
            <NavItem eventKey={2} href='#'>You all suck, I rule</NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <div className='container'>
        <Row>
          <Col>
            {children}
          </Col>
        </Row>
      </div>
    </div>
  )
}

CoreLayout.propTypes = {
  children: PropTypes.element
}

export default CoreLayout
