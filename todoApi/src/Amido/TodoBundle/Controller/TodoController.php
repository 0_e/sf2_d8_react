<?php

namespace Amido\TodoBundle\Controller;

use Amido\TodoBundle\Entity\Todo;
use Amido\TodoBundle\Form\TodoType;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TodoController extends FOSRestController
{
    /**
     * @return Todo
     */
    public function getTodoAction(Todo $todo)
    {
        return $todo;
    }

    /**
    * @return Todo[]
    */
    public function getTodosAction()
    {
        $todos = $this
            ->getDoctrine()
            ->getRepository('AmidoTodoBundle:Todo')
            ->findAll();

        return $todos;
    }

    private function treatAndValidateRequest(Todo $todo, Request $request)
    {
        $form = $this->createForm(
            TodoType::class,
            $todo,
            array(
                'method' => $request->getMethod()
            )
        );

        $form->handleRequest($request);

        $errors = $this->get('validator')->validate($todo);
        return $errors;
    }

    public function postTodosAction(Request $request)
    {
        $todo = new Todo();
        $errors = $this->treatAndValidateRequest($todo, $request);

        if (count($errors) > 0) {
            return new View(
                $errors,
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($todo);
        $manager->flush();

        return new View($todo, Response::HTTP_CREATED);
    }

    public function deleteTodoAction(Todo $todo)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($todo);
        $manager->flush();

        return new View($todo, Response::HTTP_OK);
    }
}
